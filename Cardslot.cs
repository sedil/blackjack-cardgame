using System;
using System.Collections.Generic;

namespace blackjack_cardgame {

    /// <summary>
    /// represents a slot which contains cards to examine who wins
    /// a blackjack game
    /// </summary>
    public class Cardslot {

        public List<Card> Slotcards {get;set;}

        public Cardslot(){
            Slotcards = new List<Card>();
        }

        /// <summary>
        /// Adds one card into the cardslot if the player or the bank
        /// wants to have a card
        /// <summary>
        /// <param name="card">a card from the cardstack</param>
        public void AddCard(Card card){
            Slotcards.Add(card);
        }

        /// <summary>
        /// calculates the sum of this slot to determine the winner
        /// </summary>
        /// <returns> the slotsum </returns>
        public uint GetSlotSum(){
            uint sum = 0;
            Slotcards.ForEach(card => sum += card.VALUE);
            return sum;
        }

        /// <summary>
        /// Prints all cards from this slot to the console
        /// </summary>
        public void PrintSlot(){
            Slotcards.ForEach(card => card.PrintCard());
        }

    }
}
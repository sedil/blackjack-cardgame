using System;
using System.Collections.Generic;

namespace blackjack_cardgame {

    public class Cardstack {

        public List<Card> Cardlist {get;set;}

        /// <summary>
        /// creates the cardstack and all gamecards
        /// </summary>
        public Cardstack(){
            Cardlist = new List<Card>();
            CreateCards();
        }

        /// <summary>
        /// creates all 52 gamecards for blackjack
        /// </summary>
        private void CreateCards(){
            for(uint n = 0; n < 4; n++){
                for(uint m = 1; m <= 13; m++){
                    switch(m){
                        case 1:
                            Cardlist.Add(new Card(m,"A"));
                            break;
                        case 11:
                            Cardlist.Add(new Card(10,"J"));
                            break;
                        case 12:
                            Cardlist.Add(new Card(10,"Q"));
                            break;
                        case 13:
                            Cardlist.Add(new Card(10,"K"));
                            break;
                        default:
                            Cardlist.Add(new Card(m,m.ToString()));
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// shuffles all cards from the cardstack
        /// </summary>
        public void ShuffleCards(){
            for(uint n = 0; n < 300; n++){
                int index = new Random().Next(0, Cardlist.Count - 1);
                Card temp = Cardlist[index];
                Cardlist.RemoveAt(index);
                Cardlist.Add(temp);
            }
        }

        /// <summary>
        /// returns the first card from the cardstack and add it to one of the cardslots
        /// </summary>
        /// <param name="slot"> the slot in which a card will be added</param>
        public bool AddCardFromCardstackToSlot(Cardslot slot){
            if(Cardlist.Count > 0){
                Card temp = Cardlist[0];
                Cardlist.RemoveAt(0);

                // As has a value of 1 or 11 //
                if(temp.IMAGE == "A" && slot.GetSlotSum() <= 10){
                    slot.AddCard(new Card(11,"A"));
                } else if(temp.IMAGE == "A" && slot.GetSlotSum() > 10){
                    slot.AddCard(temp);
                } else {
                    slot.AddCard(temp);
                }
                return true;
            } else {
                return false;
            }
        }

    }
}
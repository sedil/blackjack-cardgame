﻿using System;

namespace blackjack_cardgame {
    public class Game {

        private Cardstack cardstack;
        private Cardslot playerslot, bankslot;

        /// <summary>
        /// Creates all instances like Cardstack and Cardslots for the game blackjack
        /// </summary>
        public Game(){
            cardstack = new Cardstack();
            playerslot = new Cardslot();
            bankslot = new Cardslot();
        }

        /// <summary>
        /// The main game loop
        /// </summary>
        public void Gameloop(){
            while(true){
                cardstack.ShuffleCards();
                char playerstatus = GiveCards(true);
                char bankstatus = GiveCards(false);

                Console.WriteLine("PLAYERSTATUS " + playerstatus);
                Console.WriteLine("BANKSTATUS " + bankstatus);

                if(!RulesAfterGame(playerstatus,bankstatus)){
                    break;
                }

                if(Continiue()){
                    playerslot.Slotcards.ForEach(card => {
                    if(card.IMAGE == "A"){
                            cardstack.Cardlist.Add(new Card(1,"A"));
                        } else {
                            cardstack.Cardlist.Add(card);
                        }
                    });

                    bankslot.Slotcards.ForEach(card => {
                    if(card.IMAGE == "A"){
                            cardstack.Cardlist.Add(new Card(1,"A"));
                        } else {
                            cardstack.Cardlist.Add(card);
                        }
                    });

                    playerslot.Slotcards.Clear();
                    bankslot.Slotcards.Clear();
                } else {
                    break;
                }
            }
        }

        /// <summary>
        /// Asks the player if a new rounds should be played
        /// </summary>
        /// <returns> yes or no </returns>
        private bool Continiue(){
            Console.WriteLine("A new Game? (y/n)");
            char input = Console.ReadLine()[0];
            return input == 'y' ? true : false;
        }

        /// <summary>
        /// Asks if the player or the bank wants to have an extracards for their cardslot
        /// </summary>
        /// <returns> true if one actor wants to have an extra card </returns>
        private bool ExtraCard(string player){
            Console.WriteLine(player +" : Do you want an extra card? (y/n)");
            var input = Console.ReadLine()[0];
            return input == 'y' ? true : false;
        }

        /// <summary>
        /// examines if the slotsum is more than 21, 21 or lower than 31 during the game
        /// </summary>
        /// <param name="slot">the slot which will be examined</param>
        /// <returns> L = bust, B = blackjack S = sum </returns>
        private char RulesDuringGame(Cardslot slot){
            // more then 21 //
            if(slot.GetSlotSum() > 21){
                return 'L';
            } else if(slot.GetSlotSum() == 21){
                return 'B';
            } else {
                return 'S';
            }
        }

        /// <summary>
        /// examines the rules after player and bank got cards
        /// </summary>
        /// <param name="playerstatus">status of playerslot</param>
        /// <param name="bankstatus">status of bankslot</param>
        /// <returns>true, if there can be continued when player wins</returns>
        private bool RulesAfterGame(char playerstatus, char bankstatus){
            if(bankstatus == 'L'){
                Console.WriteLine("Bank looses, Bust");
                return true;
            }

            if(playerstatus == 'L'){
                Console.WriteLine("Player looses, Bust");
                return false;
            }

            if(playerstatus == 'B' && bankstatus == 'S'){
                Console.WriteLine("Player wins, Black Jack");
                return true;
            }

            if(playerstatus == 'S' && bankstatus == 'B'){
                Console.WriteLine("Bank wins, Black Jack");
                return false;
            }

            if(playerslot.GetSlotSum() > bankslot.GetSlotSum()){
                Console.WriteLine("Player " + playerslot.GetSlotSum() + " : " + bankslot.GetSlotSum() + " Bank");
                Console.WriteLine("Player wins");
                return true;
            } else if(playerslot.GetSlotSum() == bankslot.GetSlotSum()){
                Console.WriteLine("Player " + playerslot.GetSlotSum() + " : " + bankslot.GetSlotSum() + " Bank");
                Console.WriteLine("Patt");
                return true;
            } else {
                Console.WriteLine("Player " + playerslot.GetSlotSum() + " : " + bankslot.GetSlotSum() + " Bank");
                Console.WriteLine("Bank wins");
                return false;
            }
        }

        /// <summary>
        /// Handles if the player or the bank gets a card to their slots
        /// </summary>
        /// <param name="isPlayer">player</param>
        private char GiveCards(bool isPlayer){
            if(isPlayer){
                // give player 2 cards for the beginning //
                cardstack.AddCardFromCardstackToSlot(playerslot);
                cardstack.AddCardFromCardstackToSlot(playerslot);
                playerslot.PrintSlot();
                Console.WriteLine("Sum : " + playerslot.GetSlotSum());
                Console.WriteLine();

                while(true){
                    // if player wants to have an extracard
                    if(ExtraCard("Player")){
                        cardstack.AddCardFromCardstackToSlot(playerslot);
                        playerslot.PrintSlot();
                        Console.WriteLine("Sum : " + playerslot.GetSlotSum());
                        var status = RulesDuringGame(playerslot);
                        // If Bust or Blackjack
                        if(status == 'L'){
                            Console.WriteLine("Bust!");
                            Console.WriteLine();
                            return status;
                        } else if(status == 'B'){
                            Console.WriteLine("Black Jack!");
                            Console.WriteLine();
                            return status; 
                        }
                    } else {
                        Console.WriteLine();
                        return 'S';
                    }
                }
            } else {
                cardstack.AddCardFromCardstackToSlot(bankslot);
                cardstack.AddCardFromCardstackToSlot(bankslot);
                bankslot.PrintSlot();
                Console.WriteLine("Sum : " + bankslot.GetSlotSum());
                Console.WriteLine();

                while(true){
                    // if player wants to have an extracard
                    if(ExtraCard("Bank")){
                        cardstack.AddCardFromCardstackToSlot(bankslot);
                        bankslot.PrintSlot();
                        Console.WriteLine("Sum : " + bankslot.GetSlotSum());
                        var status = RulesDuringGame(bankslot);
                        // If Bust or Blackjack
                        if(status == 'L'){
                            Console.WriteLine("Bust!");
                            Console.WriteLine();
                            return status;
                        } else if(status == 'B'){
                            Console.WriteLine("Black Jack!");
                            Console.WriteLine();
                            return status; 
                        }
                    } else {
                        Console.WriteLine();
                        return 'S';
                    }
                }
            }
        }

        public static void Main(string[] args) {
            Game game = new Game();
            game.Gameloop();
        }
    }
}

using System;

namespace blackjack_cardgame {

    /// <summary>
    /// A class which creates a card for the blackjack game
    /// </summary>
    public class Card {

        private string _IMAGE;
        public string IMAGE {
            get {
                return _IMAGE;
            }
        }

        private uint _VALUE;
        public uint VALUE {
            get{
                return _VALUE;
            }
        }

        /// <summary>
        /// Creatates a card
        /// </summary>
        /// <param name="value"> cardvalue </param>
        /// <param name="image"> cardimage </param>
        public Card(uint value, string image){
            this._VALUE = value;
            this._IMAGE = image;
        }

        /// <summary>
        /// Prints this card to the console
        /// </summary>
        public void PrintCard(){
            Console.WriteLine("[" + this._IMAGE + "," + this._VALUE + "]");
        }

    }

}